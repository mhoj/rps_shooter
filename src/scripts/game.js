import { GameConfig } from './constants.js';
// scenes
import BootScene from './scenes/bootScene.js';
import PlayScene from './scenes/playScene.js';

const config = {
  type: Phaser.AUTO,
  width: GameConfig.canvasWidth,
  height: GameConfig.canvasHeight,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 0 },
      debug: true,
    },
  },
  scene: [BootScene, PlayScene],
};

new Phaser.Game(config);
