import { PlayerConfig, AvatarConfig } from '../constants.js';
import Bullet from './Bullet.js';

export default class Player extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y) {
    const initialFrame = 0;
    super(scene, x, y, 'avatars', initialFrame);
    scene.add.existing(this);
    scene.physics.add.existing(this);
    // display and physics
    this.setDisplaySize(PlayerConfig.displayWidth, PlayerConfig.displayHeight);
    this.setOrigin(AvatarConfig.originX, AvatarConfig.originY);
    this.setCircle(
      AvatarConfig.bodyRadius,
      AvatarConfig.bodyOffsetX,
      AvatarConfig.bodyOffsetY
    );
    //this.setCollideWorldBounds(true);
    // player status
    this.avatar = initialFrame;
    this.health = PlayerConfig.initialHeath;
    this.absVelocity = PlayerConfig.initialVelocity;
  }

  switchAvatar(alt) {
    this.avatar = (this.avatar + 3 + alt) % 3;
    this.setFrame(this.avatar);
  }

  shoot() {
    new Bullet(this.scene, this.x, this.y, this.avatar, this.rotation, true);
  }
}
