import { BulletConfig, AvatarConfig } from '../constants.js';

export default class Bullet extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y, type, rotation, isPlayer) {
    // create sprite
    super(scene, x, y, 'avatars', type);
    scene.add.existing(this);
    scene.physics.add.existing(this);
    // display and physics
    this.setDisplaySize(BulletConfig.displayWidth, BulletConfig.displayHeight);
    this.setRotation(rotation);
    this.setOrigin(AvatarConfig.originX, AvatarConfig.originY);
    this.setCircle(
      AvatarConfig.bodyRadius,
      AvatarConfig.bodyOffsetX,
      AvatarConfig.bodyOffsetY
    );

    // movement
    this.absVelocity = BulletConfig.initialVelocity;
    this.setVelocityX(this.absVelocity * Math.cos(this.rotation));
    this.setVelocityY(this.absVelocity * Math.sin(this.rotation));
  }
}
