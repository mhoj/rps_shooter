import { GameConfig } from '../constants.js';
import Player from '../sprites/Player.js';

export default class playScene extends Phaser.Scene {
  constructor() {
    super('playScene');
    this.level = 1;
  }

  // load game images and audios
  preload() {}

  create() {
    this.add.text(20, 20, 'Playing game...'); // loading page
    // Create Player
    this.player = new Player(
      this,
      GameConfig.mapWidth / 2,
      GameConfig.mapHeight / 2
    );
    console.log;
    // Camera
    const camera = this.cameras.main;
    camera.setBounds(0, 0, GameConfig.mapWidth, GameConfig.mapHeight);
    camera.startFollow(
      this.player,
      false,
      GameConfig.cameraLerp,
      GameConfig.cameraLerp
    );

    // Keyboards
    this.cursorKeys = this.input.keyboard.addKeys({
      up: Phaser.Input.Keyboard.KeyCodes.W,
      down: Phaser.Input.Keyboard.KeyCodes.S,
      left: Phaser.Input.Keyboard.KeyCodes.A,
      right: Phaser.Input.Keyboard.KeyCodes.D,
    });
    this.swicherKeys = this.input.keyboard.addKeys({
      prev: Phaser.Input.Keyboard.KeyCodes.O,
      next: Phaser.Input.Keyboard.KeyCodes.P,
    });

    // Mouse click
    this.input.on(
      'pointerdown',
      (pointer) => {
        this.onMouseClick(pointer);
      },
      this
    );
  }

  onMouseClick(pointer) {
    this.player.shoot();
  }

  update() {
    // Player move
    this.player.setVelocity(0);

    if (this.cursorKeys.left.isDown) {
      this.player.setVelocityX(-this.player.absVelocity);
    } else if (this.cursorKeys.right.isDown) {
      this.player.setVelocityX(this.player.absVelocity);
    }

    if (this.cursorKeys.up.isDown) {
      this.player.setVelocityY(-this.player.absVelocity);
    } else if (this.cursorKeys.down.isDown) {
      this.player.setVelocityY(this.player.absVelocity);
    }

    // Player rotation
    const pointer = this.input.activePointer;
    const radans = Phaser.Math.Angle.Between(
      this.player.x,
      this.player.y,
      pointer.x,
      pointer.y
    );
    this.player.setRotation(radans);

    // Switch player avatar
    if (Phaser.Input.Keyboard.JustDown(this.swicherKeys.prev)) {
      this.player.switchAvatar(-1);
    }
    if (Phaser.Input.Keyboard.JustDown(this.swicherKeys.next)) {
      this.player.switchAvatar(1);
    }

    // Player Shoot
  }

  initLevel() {
    // generate map
    // add player
    // spawn enemy
  }
}
