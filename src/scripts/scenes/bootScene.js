export default class bootScene extends Phaser.Scene {
  constructor() {
    super('bootScene');
  }

  // load game images and audios
  preload() {
    this.load.spritesheet('avatars', 'assets/spritesheets/avatars.png', {
      frameWidth: 200,
      frameHeight: 200,
    });
  }

  create() {
    // create animations
    this.add.text(20, 20, 'Loading game...'); // loading page
    this.scene.start('playScene'); // done loading, go to next scene
  }
}
