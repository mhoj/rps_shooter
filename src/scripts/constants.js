export const GameConfig = {
  canvasWidth: 800,
  canvasHeight: 600,
  mapWidth: 1000,
  mapHeight: 1000,
  cameraLerp: 0.2,
};

export const AvatarConfig = {
  originX: 0.32,
  originY: 0.5,
  bodyRadius: 48,
  bodyOffsetX: 16,
  bodyOffsetY: 60,
};

export const PlayerConfig = {
  displayWidth: 48,
  displayHeight: 48,
  initialHeath: 3,
  initialVelocity: 200,
};

export const BulletConfig = {
  displayWidth: 30,
  displayHeight: 30,
  initialVelocity: 400,
};

export const BulletKeys = ['Rock', 'Paper', 'Sissors'];
